# Container images

This repository contains container images used by Tails.

Based on: https://gitlab.torproject.org/tpo/tpa/base-images

## Bootstrap

All builds need a base image to start from, so manual bootstrap of a Debian
Bookworm and a Podman image are required.

To manually build the initial Debian Bookworm image (make sure to replace CI
user/pass accordingly):

```
cd debian/
export SUITE=bookworm
export CI_JOB_NAME=bookworm
export CI_REGISTRY_IMAGE=registry.gitlab.tails.boum.org/sysadmin-team/container-images
export CI_REGISTRY=registry.gitlab.tails.boum.org
export CI_REGISTRY_USER=$CI_REGISTRY_USER
export CI_REGISTRY_PASSWORD=$CI_REGISTRY_PASSWORD
./build.sh
```

To manually build the initial Podman image:

```
REGISTRY=registry.gitlab.tails.boum.org
TAG=${REGISTRY}/sysadmin-team/container-images/podman:bookworm
podman build -t ${TAG} ./podman
podman login ${REGISTRY}
podman push ${TAG}
```

## Reproducibility

Before pushing newly built images to the registry, we compare their rootfs
checksums with the ones from the latest images in the registry. If they match,
we don't upload.

In order for this to work as expected, some care needs to be taken in the
corresponding Containerfiles.

For example, if apt-get is used, you'll at least need to add the following to
the Containerfile:

```
RUN apt-get clean && \
  rm -rf /var/cache/ldconfig/aux-cache \
    /var/lib/apt/lists/* \
    /var/lib/dbus/machine-id \
    /var/log/alternatives.log \
    /var/log/apt/eipp.log.xz \
    /var/log/apt/history.log \
    /var/log/apt/term.log \
    /var/log/dpkg.log
```

You may need to take extra measures to ensure the contents of rootfs are always
the same.
