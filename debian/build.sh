#!/bin/bash

# XXX: this should be re-written to be part of our installer, possibly
# as a submodule of tsa-misc, but maybe not in fabric

set -u

# The mmdebstrap(1) script will automatically add the typical debian mirrors,
# but if you wish to add an additional mirror (such as backports), you can pass
# the environment variable ADD_MIRROR to add a debian mirror to
# /etc/apt/sources.list.d/custom.list.
#
# example in .gitlab-ci.yml:
#
# bullseye-backports:
#  stage: build
#  variables:
#    SUITE: bullseye
#    ADD_MIRROR: "deb http://deb.debian.org/debian bullseye-backports main"
#  <<: *build_debian_image
#
# bash-specific
set -o pipefail

# the parameters for the image

# where to fetch the packages from
# This is not necessary, its the default, and if you use the default
# the security mirror is setup automatically
#MIRROR="http://deb.debian.org/debian"

# this will really always be "debian", but might tolerate "ubuntu" if
# you (a) change the mirror above, (b) the suite, (c) the variant,
# and, more importantly, if mmdebootstrap supports it
IMAGE=debian
# The suite is controlled by the environment variable in .gitlab-ci.yml
VARIANT=minbase
RUNTIME=${RUNTIME:-podman}

# No servicable parts below this line

# fetch the last update time of the mirror to create a reproducible
# tarball based on that date
ORIG_MIRROR=${MIRROR:-http://deb.debian.org/debian}
MIRROR_DATE=$(curl -s ${ORIG_MIRROR}/dists/${SUITE}/Release | /usr/bin/grep-dctrl -s Date -n '')
echo "I: mirror $ORIG_MIRROR timestamp is $MIRROR_DATE" >&2
# to have this reproducible
SOURCE_DATE_EPOCH=$(date --date="$MIRROR_DATE" +%s)
export SOURCE_DATE_EPOCH
echo "I: epoch is $SOURCE_DATE_EPOCH seconds" >&2

MODE=auto
TAG=$CI_JOB_NAME-$VARIANT

# this creates an image similar to the official ones built by
# debuerreotype, except that instead of needing a whole set of scripts
# and hacks, we only rely on mmdebstrap.
#
# the downside is that the image is not reproducible (even if ran
# against the same mirror without change), mainly because `docker
# import` stores the import timestamp inside the image. however, the
# internal checksum itself should be reproducible.

mmdebstrap \
  --mode=$MODE \
  --variant=$VARIANT \
  --aptopt='Acquire::Languages "none"' \
  --dpkgopt='path-exclude=/usr/share/man/*' \
  --dpkgopt='path-exclude=/usr/share/man/man[1-9]/*' \
  --dpkgopt='path-exclude=/usr/share/locale/*' \
  --dpkgopt='path-include=/usr/share/locale/locale.alias' \
  --dpkgopt='path-exclude=/usr/share/lintian/*' \
  --dpkgopt='path-exclude=/usr/share/info/*' \
  --dpkgopt='path-exclude=/usr/share/doc/*' \
  --dpkgopt='path-include=/usr/share/doc/*/copyright' \
  --dpkgopt='path-exclude=/usr/share/omf/*' \
  --dpkgopt='path-exclude=/usr/share/help/*' \
  --dpkgopt='path-exclude=/usr/share/gnome/*' \
  --dpkgopt='path-exclude=/usr/share/examples/*' \
  --include='ca-certificates' \
  --setup-hook='cp minimize-config/dpkg.cfg.d/* "$1/etc/dpkg/dpkg.cfg.d/"' \
  --setup-hook='cp minimize-config/apt.conf.d/* "$1/etc/apt/apt.conf.d/"' \
  ${ADD_MIRROR:+--setup-hook='echo "'"$ADD_MIRROR"'" >  "$1"/etc/apt/sources.list.d/custom.list'} \
  --customize-hook='rm "$1"/etc/resolv.conf' \
  --customize-hook='rm "$1"/etc/hostname' \
  $SUITE  - $ORIG_MIRROR |  $RUNTIME import -c 'CMD ["bash"]' - $IMAGE:$TAG
if [ $? -ne 0 ]; then
    echo "E: failed to build new image" >&2
    exit 1
fi

# Determine if this newly built image is different from the one in the registry.
# If it is different, then something has updated, and we want the newer version in the registry.
# If it is not different, then we do not want to push this newer version. The
# reason why we don't want to push the newer version is because the container
# digest changes each time, even if the contents are the same. This is due to
# some timestamps that `podman import` puts in there, and no amount of
# libfaketime has been able to change that. (see
# https://github.com/containers/podman/issues/14978#issuecomment-1750655731)

# Get digest of the top layer of the RootFS of the new image and compare it to
# the existing image in the registry. The image built from mmdebstrap only has
# one layer, but this allows us to pull the value from the array

# This is the image name on the registry.
registry_tag="$CI_REGISTRY_IMAGE/$IMAGE:$CI_JOB_NAME"
registry_tag_date="${CI_REGISTRY_IMAGE}/${IMAGE}:${CI_JOB_NAME}-$(date '+%Y%m%d%H%M')"

cur_digest=$($RUNTIME inspect --type image --format "{{index .RootFS.Layers 0}}" $IMAGE:$TAG)
prev_digest=$(skopeo inspect --config --raw "docker://${registry_tag}" | jq -r '.rootfs.diff_ids[0]')

echo "New image RootFS digest: $cur_digest"
echo "Registry RootFS digest: $prev_digest"

push_image()
{
    tag="$1"
    echo "Pushing new image to registry as $tag"
    if ! $RUNTIME push "$IMAGE:$TAG" "$tag"; then
        echo "E: failed to push image" >&2
        exit 1
    fi
}

if [ "$cur_digest" != "$prev_digest" ]
then
    podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    for tag in $registry_tag $registry_tag_date; do
        push_image "$tag"
    done
    if [ "$SUITE" = "$(distro-info --stable)" ]; then
        for tag in "$CI_REGISTRY_IMAGE/$IMAGE:stable" "$CI_REGISTRY_IMAGE/$IMAGE:latest"; do
            push_image "$tag"
        done
    fi
else
    echo "Newly built image matches current registry image, not pushing"
fi

# note that we could also have used `| tar --delete /$PATH` to exclude files
